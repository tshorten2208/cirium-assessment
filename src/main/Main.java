package main;

import main.data.*;

import java.time.Duration;
import java.time.Instant;

public class Main {
    public static void main(String[] args) {
        Aerodome A = new Aerodome("start", new Coordinate(10, 10));
        Aerodome B = new Aerodome("end", new Coordinate(0, 0));

        Instant departTime = Instant.now().minus(Duration.ofHours(3));
        Instant arrivalTime = Instant.now();

        Flight flight = new Flight(B, arrivalTime, A, departTime);

        Instant flightTime = Instant.now().minus(Duration.ofHours(1));

        Coordinate position = flight.getPosition(flightTime);

        System.out.println(position.getX());
        System.out.println(position.getY());

        Coordinate airspaceBottomLeft = new Coordinate(2, 2);
        Coordinate airspaceTopRight = new Coordinate(6, 6);

        Airspace airspace = new Airspace(airspaceBottomLeft, airspaceTopRight);

        System.out.println(flight.inAirspace(airspace, flightTime));

        if(flight.passesThroughAirspace(airspace)) {
            System.out.println("Flight path intercepts airspace");
        } else {
            System.out.println("Flight path does not intercept airspace");
        }
    }
}