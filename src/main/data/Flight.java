// Copyright (C) 2022 Cirium. All rights reserved.
package main.data;

import java.time.Duration;
import java.time.Instant;

/**
 * The class representing a Flight.
 */
public class Flight {

    /** The aerodrome the flight is arriving at. */
    private final Aerodome arrivalAerodrome;
    /** The date/time the flight is arriving. */
    private final Instant arrivalTime;

    /** The aerodrome the flight is departing from. */
    private final Aerodome departureAerodrome;

    /** The date/time the flight is departing. */
    private final Instant departureTime;

    /** The path the flight follows. */
    private final Path path;

    /**
     * Construct a flight.
     *
     * @param arrivalAerodrome   The aerodrome the flight is arriving at.
     * @param arrivalTime        The date/time the flight is arriving.
     * @param departureAerodrome The aerodrome the flight is departing from.
     * @param departureTime      The date/time the flight is departing.
     */
    public Flight(final Aerodome arrivalAerodrome, final Instant arrivalTime, final Aerodome departureAerodrome, final Instant departureTime) {
        this.arrivalAerodrome = arrivalAerodrome;
        this.arrivalTime = arrivalTime;
        this.departureAerodrome = departureAerodrome;
        this.departureTime = departureTime;
        this.path = new Path(departureAerodrome.getLocation(), arrivalAerodrome.getLocation());
    }

    /**
     * Gets the aerodrome the flight is arriving at.
     *
     * @return the arrival aerodrome.
     */
    public Aerodome getArrivalAerodrome() {
        return arrivalAerodrome;
    }

    /**
     * Gets the date/time the flight is arriving.
     *
     * @return the arrival time.
     */
    public Instant getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Gets the aerodrome the flight is departing from.
     *
     * @return the departure aerodrome.
     */
    public Aerodome getDepartureAerodrome() {
        return departureAerodrome;
    }

    /**
     * Gets the date/time the flight is departing.
     *
     * @return the departure time.
     */
    public Instant getDepartureTime() {
        return departureTime;
    }

    /**
     * Computes the horizontal velocity of the flight.
     *
     * @return the horizontal velocity.
     */
    private double horizontalVelocity() {
        double deltaX = arrivalAerodrome.getX() - departureAerodrome.getX();
        Duration duration = Duration.between(departureTime, arrivalTime);
        double deltaT = duration.getSeconds();
        return deltaX / deltaT;
    }

    /**
     * Computes the vertical velocity of the flight.
     *
     * @return the vertical velocity.
     */
    private double verticalVelocity() {
        double deltaY = arrivalAerodrome.getY() - departureAerodrome.getY();
        Duration duration = Duration.between(departureTime, arrivalTime);
        double deltaT = duration.getSeconds();
        return deltaY / deltaT;
    }

    /**
     * Computes the position of the flight.
     *
     * @param flightTime the instant at which to calculate the flight position.
     * @return the flight position.
     */
    public Coordinate getPosition(Instant flightTime) {
        if (flightTime.isAfter(arrivalTime)) {
            return arrivalAerodrome.getLocation();
        } else if (flightTime.isBefore(departureTime)) {
            return departureAerodrome.getLocation();
        } else {
            Duration duration = Duration.between(departureTime, flightTime);
            return getPosition(duration.getSeconds());
        }
    }

    /**
     * Computes the position of the flight.
     *
     * @param flightDuration the duration (in seconds) of time the flight has been in progress.
     * @return the flight position.
     */
    private Coordinate getPosition(double flightDuration) {
        double horizontalDistance = horizontalVelocity() * flightDuration;
        double horizontalPosition = horizontalDistance + departureAerodrome.getX();
        double verticalDistance = verticalVelocity() * flightDuration;
        double verticalPosition = verticalDistance + departureAerodrome.getY();
        return new Coordinate(horizontalPosition, verticalPosition);
    }

    /**
     * Checks if the flight passes through a given airspace.
     *
     * @param airspace the airspace which is checked.
     * @return true if flight passes through the airpsace.
     */
    public boolean passesThroughAirspace(Airspace airspace) {
        return airspace.flightPathIntercepts(path);
    }

    /**
     * Checks if the flight is within a given airspace.
     *
     * @param airspace the airspace which is checked.
     * @param time     the instant at which to calculate the flight position.
     * @return true if flight is in the airpsace.
     */
    public boolean inAirspace(Airspace airspace, Instant time) {
        Coordinate flightPosition = getPosition(time);
        return airspace.containsPosition(flightPosition);
    }
}
