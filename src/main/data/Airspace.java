// Copyright (C) 2022 Cirium. All rights reserved.
package main.data;

/**
 * The class representing an Airspace.
 */
public class Airspace {

    /** The bottom left coordinate of this airspace. */
    private final Coordinate bottomLeft;

    /** The top right coordinate of this airspace. */
    private final Coordinate topRight;

    /**
     * Construct an Airspace.
     *
     * @param bottomLeft the bottom left coordinate of this airspace.
     * @param topRight   the top right coordinate of this airspace.
     */
    public Airspace(final Coordinate bottomLeft, final Coordinate topRight) {
        this.bottomLeft = bottomLeft;
        this.topRight = topRight;
    }

    /**
     * Gets the bottom left coordinate of this airspace.
     *
     * @return the bottom left.
     */
    public Coordinate getBottomLeft() {
        return bottomLeft;
    }

    /**
     * Gets the top right coordinate of this airspace.
     *
     * @return the top right.
     */
    public Coordinate getTopRight() {
        return topRight;
    }

    /**
     * Checks if a given position is within the airspace.
     *
     * @param coordinate The coordinate to check.
     * @return true if the given coordinate is within the airspace.
     */
    public boolean containsPosition(Coordinate coordinate) {
        return containsX(coordinate.getX()) && containsY(coordinate.getY());
    }

    /**
     * Check if the given X point is within the airspace
     *
     * @param x The horizontal coordinate to check
     * @return true if x is within the airpace
     */
    private boolean containsX(double x) {
        return x <= topRight.getX() && x >= bottomLeft.getX();
    }

    /**
     * Check if the given Y point is within the airspace
     *
     * @param y The vertical coordinate to check
     * @return true if y is within the airpace
     */
    private boolean containsY(double y) {
        return y <= topRight.getY() && y >= bottomLeft.getY();
    }

    /**
     * Check if the flight path intercepts any sides of the aispace
     *
     * @param flightPath The flight path to check
     * @return true if flightPath intercepts any edge
     */
    public boolean flightPathIntercepts(Path flightPath) {
        return flightPath.interceptsX(bottomLeft.getX(), bottomLeft.getY(), topRight.getY())
                || flightPath.interceptsX(topRight.getX(), bottomLeft.getY(), topRight.getY())
                || flightPath.interceptsY(bottomLeft.getY(), bottomLeft.getX(), topRight.getX())
                || flightPath.interceptsY(topRight.getY(), bottomLeft.getX(), topRight.getX());

    }
}
