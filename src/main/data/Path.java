package main.data;

/**
 * The class representing a flight path.
 */
public class Path {
    /**
     * The start point of the path
     */
    private final Coordinate start;
    /**
     * The end point of the path
     */
    private final Coordinate end;

    /**
     * Construct a path.
     *
     * @param start the start coordinate of the path.
     * @param end   the end coordinate of the path.
     */
    public Path(Coordinate start, Coordinate end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Computes the path gradient, where the gradient (m) is represented by y = mx + c.
     *
     * @return the gradient, m.
     */
    private double gradient() {
        return (start.getY() - end.getY()) / (start.getX() - end.getX());
    }

    /**
     * Computes the path constant, where the constant (c) is represented by y = mx + c.
     *
     * @return the constant, c.
     */
    private double constant() {
        return start.getY() - start.getX() * (gradient());
    }

    /**
     * Computes the Y coordinate along the path, given any X coordinate.
     *
     * @param x the x coordinate.
     * @return the corresponding Y coordinate.
     */
    public double getY(double x) {
        return gradient() * x + constant();
    }

    /**
     * Computes the X coordinate along the path, given any Y coordinate.
     *
     * @param y the y coordinate.
     * @return the corresponding X coordinate.
     */
    public double getX(double y) {
        return (y - constant()) / gradient();
    }

    /**
     * Checks if the path crosses a line, represented by Y = C.
     *
     * @param c  the constant, C, that represents the straight line.
     * @param y1 the lower bound of the line.
     * @param y2 the upper bound of the line.
     * @return true if the path intercepts the line.
     */
    public boolean interceptsX(double c, double y1, double y2) {
        double yPosition = getY(c);
        return yPosition <= y2 && yPosition >= y1;
    }

    /**
     * Checks if the path crosses a line, represented by X = C.
     *
     * @param c  the constant, C, that represents the straight line.
     * @param x1 the lower bound of the line.
     * @param x2 the upper bound of the line.
     * @return true if the path intercepts the line.
     */
    public boolean interceptsY(double c, double x1, double x2) {
        double xPosition = getX(c);
        return xPosition <= x2 && xPosition >= x1;
    }
}
