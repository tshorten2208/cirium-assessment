package main.data;

/**
 * The class representing an Aerodome.
 */
public class Aerodome {
    /** The id / name of the aerodome. */
    private final String id;
    /** The coordinate of the flight aerodome. */
    private final Coordinate location;

    /**
     * @param id       The name / id of the aerodome.
     * @param location The aerodome's location.
     */
    public Aerodome(final String id, final Coordinate location) {
        this.id = id;
        this.location = location;
    }

    /**
     * Gets the aerodome id.
     *
     * @return the aerodome id.
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the aerodome coordinates / location.
     *
     * @return the aerodome location.
     */
    public Coordinate getLocation() {
        return location;
    }

    /**
     * Gets the X coordinate of the aerodome location.
     *
     * @return the aerodome X coordinate.
     */
    public double getX() {
        return location.getX();
    }

    /**
     * Gets the Y coordinate of the aerodome location.
     *
     * @return the aerodome Y coordinate.
     */
    public double getY() {
        return location.getY();
    }
}
